---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

local json = require "json"
local filename = system.pathForFile( "images.json", system.ResourceDirectory )
local t, pos, msg= json.decodeFile(filename)
local mssg = ""
local hth = 0

if not t then
	mssg = "ошибка парсинга " --..tostring(pos)..": "..tostring(msg)
else
	mssg = "файл успешно открыт..."
end


-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

local widget = require( "widget" )

local nextSceneButton
local nextSceneButtonText
local fileStatusText

local scrollView = widget.newScrollView
		{
			left = 0,
			top = 40,
			topPadding = 65,
			width = display.contentWidth,
			height = display.contentHeight-40,
			horizontalScrollDisabled = true,
			verticalScrollDisabled = false,
			listener = scrollListener,
			hideBackground = false
		}


function scene:create( event )
    local sceneGroup = self.view

	local function scrollListener( event )
		local phase = event.phase
		local direction = event.direction
	
		if event.limitReached then
			if "up" == direction then
				print( "Reached top limit" )
			elseif "down" == direction then
				print( "Reached Bottom limit" )
			end
		end
	
		return true
	end

	nextSceneButton = self:getObjectByName( "GoToScene1Btn" )
	nextSceneButtonText = self:getObjectByName( "GoToScene1Text" )
	fileStatusText = self:getObjectByName( "fileStatusText" )
	
	--fileStatusText.text = mssg
	
	fileStatusText.y = display.contentHeight - 10
	
	local function networkListener( event )
		if ( event.isError ) then
            print ( "Network error - download failed" )
		else
            event.target.alpha = 0
			transition.to( event.target, { alpha = 1.0 } )
			event.target.x = display.contentCenterX
			event.target.width = display.contentWidth-40
        	event.target.y = hth
			hth = hth + event.target.height + 50
            scrollView:insert(event.target)
		end
	end
	
	for k,v in pairs(t.images)	do 
		display.loadRemoteImage( v, "GET", networkListener, "CopyImage"..k..".png", system.TemporaryDirectory, 0, 0 ) 
	end
		
	
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
		scrollView.isVisible = true
		scrollView.x = display.contentCenterX
        scrollView.y = display.contentCenterY
		
		function nextSceneButton:touch ( event )
       		local phase = event.phase
       		if "ended" == phase then
			    scrollView.isVisible = false
       			composer.gotoScene( "scene1", { effect = "fade", time = 150 } )
       		end
       	end
       	nextSceneButton:addEventListener( "touch", nextSceneButton )
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )

---------------------------------------------------------------------------------

return scene
