---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------


local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

---------------------------------------------------------------------------------

local nextSceneButton

function scene:create( event )
    local sceneGroup = self.view

end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
	
        local title = self:getObjectByName( "Title" )
        title.x = display.contentCenterX
        title.y = display.contentCenterY
		title.height = 400
		title:setFillColor( 0, 0, 0 )
		title.align = left
		title.text = [[Пропарсить файл формата JSON
		'images.json' и по полученным ссылкам
		вывести на экран изображения
		с возможностью пролистывать их
		(widget.ScrollView).
		Реализовать это на сцене
		(использовать composer)]]

        local goToScene2Btn = self:getObjectByName( "GoToScene2Btn" )
        goToScene2Btn.x = display.contentWidth - 155
        goToScene2Btn.y = display.contentHeight - 35
        local goToScene2Text = self:getObjectByName( "GoToScene2Text" )
        goToScene2Text.x = display.contentWidth - 155
        goToScene2Text.y = display.contentHeight - 35
    elseif phase == "did" then

        nextSceneButton = self:getObjectByName( "GoToScene2Btn" )
        if nextSceneButton then
		    -- создаем функцию для перехода к парсингу
        	function nextSceneButton:touch ( event )
        		local phase = event.phase
        		if "ended" == phase then
        			composer.gotoScene( "scene2", { effect = "fade", time = 150 } )
        		end
        	end
        	-- привязываем событие касания кнопки с функцией перехода к парсингу
        	nextSceneButton:addEventListener( "touch", nextSceneButton )
        end
        
    end 
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )

---------------------------------------------------------------------------------

return scene
